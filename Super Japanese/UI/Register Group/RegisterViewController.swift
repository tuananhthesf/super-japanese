//
//  RegisterViewController.swift
//  Super Japanese
//
//  Created by Nguyen Tuan Anh on 1/8/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPass: UITextField!
    @IBOutlet weak var tfRepass: UITextField!
    @IBOutlet weak var lbLevel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btnClosePicker: UIButton!
    
    public let dataSource = ["N1","N2","N3","N4","N5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initAvatar()
        initPickerView()
    }
    
    @IBAction func selectLevelOnclick(_ sender: UIButton) {
        pickerView.isHidden = false
        btnClosePicker.isHidden = false
    }
    
    @IBAction func regOnClick(_ sender: UIButton) {
        
        if (tfPass.text != tfRepass.text) {
            self.view.makeToast("Mật khẩu bạn nhập lại không đúng.")
        } else if (tfPass.text?.count ?? 0 < 6) {
            self.view.makeToast("Vui lòng nhập mật khẩu lớn hơn 6 kí tự.")
        } else if (tfUsername.text == "") {
            self.view.makeToast("Vui lòng nhập username")
        } else if !(validateEmail(enteredEmail: (tfEmail.text!))) {
            self.view.makeToast("Email không đúng định dạng.")
        } else if lbLevel.text == "Level" {
          self.view.makeToast("Vui lòng chọn Level")
        } else {
            let mainView = MainViewController(nibName: "MainViewController", bundle: nil)
            mainView.text = tfUsername.text!
            mainView.dismiss(animated: true, completion: nil)
            present(mainView, animated: true, completion: nil)
        }
    }
    
    @IBAction func closePicker(_ sender: UIButton) {
        pickerView.isHidden = true
        btnClosePicker.isHidden = true
        btnClosePicker.isSelected = false
    }
    
    @IBAction func backToHome(_ sender: Any) {
        let homeView = HomeViewController(nibName: "HomeViewController", bundle: nil)
        present(homeView, animated: true, completion: nil)
    }
    
    func initAvatar() {
        imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
        imgAvatar.layer.masksToBounds = true
        imgAvatar.layer.borderWidth = 2
        imgAvatar.layer.borderColor = UIColor(red:255, green:255, blue:255, alpha:1.0).cgColor
    }
    
    func initPickerView() {
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.isHidden = true
        btnClosePicker.isHidden = true
        pickerView.setValue(UIColor.white, forKeyPath: "textColor")
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]{2,}@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return enteredEmail.range(of: emailFormat, options: .regularExpression) != nil
    }

}

extension RegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lbLevel.text = " Level: \(dataSource[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = dataSource[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        return myTitle
    }
    
}
