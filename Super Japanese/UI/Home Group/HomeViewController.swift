//
//  HomeViewController.swift
//  Super Japanese
//
//  Created by Nguyen Tuan Anh on 1/8/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBAction func regOnClick(_ sender: UIButton) {
        let regView = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
        regView.dismiss(animated: true, completion: nil)
        present(regView, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
