//
//  MainViewController.swift
//  Super Japanese
//
//  Created by Nguyen Tuan Anh on 1/8/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tvHello: UILabel!
    
    let sadImage = #imageLiteral(resourceName: "traditional_japanese_sad")
    let smileImage = #imageLiteral(resourceName: "traditional_japanese_smile")
    
    var text = ""
    
    @IBAction func exitClick(_ sender: UIButton) {
    UIControl().sendAction(#selector(NSXPCConnection.suspend),
                               to: UIApplication.shared, for: nil)
    }
    
    @IBAction func okOnclick(_ sender: UIButton) {
        imageView.image = smileImage
    }
    
    @IBAction func notOkOnclick(_ sender: UIButton) {
        imageView.image = sadImage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvHello.text = "Hello \(text)"
    }
    
}
